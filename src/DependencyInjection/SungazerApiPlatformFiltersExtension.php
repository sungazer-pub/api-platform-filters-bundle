<?php

namespace Sungazer\Bundle\ApiPlatformFiltersBundle\DependencyInjection;


use Exception;
use InvalidArgumentException;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;

class SungazerApiPlatformFiltersExtension extends Extension
{

    /**
     * Loads a specific configuration.
     *
     * @throws InvalidArgumentException When provided tag is not defined in this extension
     * @throws Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();

        $config = $this->processConfiguration($configuration, $configs);
//
//        if ($config['enabled']) {
//            $loader = new XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
//            $loader->load('services.xml');
//        }
    }
}