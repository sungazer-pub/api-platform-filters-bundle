<?php

namespace Sungazer\Bundle\ApiPlatformFiltersBundle\DependencyInjection;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

// https://symfony.com/doc/current/bundles/configuration.html

class Configuration implements ConfigurationInterface
{

    /**
     * Generates the configuration tree builder.
     *
     * @return TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('sungazer_api_platform_filters');

        $rootNode = $treeBuilder->getRootNode();

        return $treeBuilder;
    }
}
