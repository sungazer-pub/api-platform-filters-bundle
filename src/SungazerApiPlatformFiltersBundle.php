<?php

namespace Sungazer\Bundle\ApiPlatformFiltersBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * This bundle gathers different filters useful for Api Platform
 *
 * Author: Luca Nardelli <luca@sungazer.io>
 */

class SungazerApiPlatformFiltersBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
    }


}

