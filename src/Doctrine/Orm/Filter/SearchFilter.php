<?php

/*
 * This file is part of the API Platform project.
 *
 * (c) Kévin Dunglas <dunglas@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Sungazer\Bundle\ApiPlatformFiltersBundle\Doctrine\Orm\Filter;

use ApiPlatform\Core\Api\FilterInterface;
use ApiPlatform\Core\Api\IdentifiersExtractorInterface;
use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\Bridge\Doctrine\Common\PropertyHelperTrait;
use ApiPlatform\Core\Bridge\Doctrine\Orm\PropertyHelperTrait as OrmPropertyHelperTrait;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryBuilderHelper;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Core\Exception\InvalidArgumentException;
use Closure;
use DateTimeInterface;
use Doctrine\DBAL\Types\Type as DBALType;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Generator;
use Psr\Log\LoggerInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;
use function count;
use function defined;
use function is_int;
use function is_string;

/**
 * Enhanced search filter for Api Platform
 *
 * Filter can be configured as follows:
 * properties = {
 *   "<queryParamName>" : {
 *     "property": "<entity property name, defaults to queryParamName>,
 *     "defaultStrategy": "exact|partial|ipartial"
 *     <other attributes here...>
 *   }
 * }
 *
 * Filter can then be invoked as follows
 * ?queryParamName[<strategy>]=<value>
 * ?queryParamName[<strategy>][]=<value>
 *
 * @author Luca Nardelli <luca@sungazer.io>
 */
class SearchFilter implements FilterInterface
{
    use PropertyHelperTrait;
    use OrmPropertyHelperTrait;

    public const STRATEGY_EXACT       = 'exact';
    public const STRATEGY_IEXACT      = 'iexact';
    public const STRATEGY_PARTIAL     = 'partial';
    public const STRATEGY_IPARTIAL    = 'ipartial';
    public const STRATEGY_START       = 'start';
    public const STRATEGY_ISTART      = 'istart';
    public const STRATEGY_END         = 'end';
    public const STRATEGY_IEND        = 'iend';
    public const STRATEGY_WORD_START  = 'word_start';
    public const STRATEGY_IWORD_START = 'iword_start';

    public const STRATEGIES = [
        self::STRATEGY_EXACT, self::STRATEGY_IEXACT,
        self::STRATEGY_PARTIAL, self::STRATEGY_IPARTIAL,
        self::STRATEGY_START, self::STRATEGY_ISTART,
        self::STRATEGY_END, self::STRATEGY_IEND,
        self::STRATEGY_WORD_START, self::STRATEGY_IWORD_START
    ];

    public const DOCTRINE_INTEGER_TYPE = DBALType::INTEGER;

    /** @var IriConverterInterface */
    protected $iriConverter;
    /** @var PropertyAccessorInterface|null */
    protected $propertyAccessor;
    /** @var IdentifiersExtractorInterface|null */
    protected $identifiersExtractor;
    /** @var array */
    protected $properties;
    /**
     * @var ManagerRegistry
     */
    private $managerRegistry;
    /**
     * @var LoggerInterface|null
     */
    private $logger;
    /**
     * @var NameConverterInterface|null
     */
    private $nameConverter;

    public function __construct(ManagerRegistry $managerRegistry, IriConverterInterface $iriConverter, PropertyAccessorInterface $propertyAccessor = null, LoggerInterface $logger = null, array $properties = null, IdentifiersExtractorInterface $identifiersExtractor = null, NameConverterInterface $nameConverter = null)
    {
//        parent::__construct($managerRegistry, $requestStack, $logger, $properties, $nameConverter);

        if (null === $identifiersExtractor) {
            @trigger_error('Not injecting ItemIdentifiersExtractor is deprecated since API Platform 2.5 and can lead to unexpected behaviors, it will not be possible anymore in API Platform 3.0.', E_USER_DEPRECATED);
        }

        $this->iriConverter         = $iriConverter;
        $this->identifiersExtractor = $identifiersExtractor;
        $this->properties           = $properties;
        $this->propertyAccessor     = $propertyAccessor ?: PropertyAccess::createPropertyAccessor();
        $this->managerRegistry      = $managerRegistry;
        $this->propertyAccessor     = $propertyAccessor;
        $this->logger               = $logger;
        $this->nameConverter        = $nameConverter;
    }

    public function getDescription(string $resourceClass): array
    {
        // TODO
        return [];
    }

    public function apply(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null, array $context = [])
    {
        $filters = $context['filters'] ?? [];

        foreach ($this->extractOperations($filters) as list($strategy, $property, $value)) {
            $this->applyFilterOperation($strategy, $property, $value, $queryBuilder, $queryNameGenerator, $resourceClass);
        }
    }

    /**
     * Extracts filter operations given a filters array and the current filter properties.
     * @param array $filters
     * @return Generator
     */
    protected function extractOperations(array $filters)
    {
        foreach ($filters as $key => $filterSpec) {
            if (!array_key_exists($key, $this->properties)) {
                continue;
            }
            // Filter configuration for property
            $propertySpec = $this->properties[$key];
            $property     = $propertySpec['property'] ?? $key; // Default value is the same as query param name
            // Process strategies
            foreach (self::STRATEGIES as $STRATEGY) {
                if (!array_key_exists($STRATEGY, $filterSpec)) {
                    continue;
                }
                $val = $filterSpec[$STRATEGY];
                yield [$STRATEGY, $property, $val];
            }
        }

    }

    protected function applyFilterOperation(string $strategy, string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass)
    {
        if (
            null === $value ||
            !$this->isPropertyMapped($property, $resourceClass, true)
        ) {
            return;
        }

        $alias = $queryBuilder->getRootAliases()[0];
        $field = $property;

        $associations = [];
        if ($this->isPropertyNested($property, $resourceClass)) {
            [$alias, $field, $associations] = $this->addJoinsForNestedProperty($property, $alias, $queryBuilder, $queryNameGenerator, $resourceClass);
        }

        $values = $this->normalizeValues((array)$value, $property);
        if (null === $values) {
            return;
        }

        $caseSensitive = true;
        $metadata      = $this->getNestedMetadata($resourceClass, $associations);

        if ($metadata->hasField($field)) {
            if ('id' === $field) {
                $values = array_map([$this, 'getIdFromValue'], $values);
            }

            if (!$this->hasValidValues($values, $this->getDoctrineFieldType($property, $resourceClass))) {
                $this->logger->notice('Invalid filter ignored', [
                    'exception' => new InvalidArgumentException(sprintf('Values for field "%s" are not valid according to the doctrine type.', $field)),
                ]);

                return;
            }

            // prefixing the strategy with i makes it case insensitive
            if (0 === strpos($strategy, 'i')) {
                $strategy      = substr($strategy, 1);
                $caseSensitive = false;
            }

            if (1 === count($values)) {
                $this->addWhereByStrategy($strategy, $queryBuilder, $queryNameGenerator, $alias, $field, $values[0], $caseSensitive);

                return;
            }

            if (self::STRATEGY_EXACT !== $strategy) {
                $this->logger->notice('Invalid filter ignored', [
                    'exception' => new InvalidArgumentException(sprintf('"%s" strategy selected for "%s" property, but only "%s" strategy supports multiple values', $strategy, $property, self::STRATEGY_EXACT)),
                ]);

                return;
            }

            $wrapCase       = $this->createWrapCase($caseSensitive);
            $valueParameter = $queryNameGenerator->generateParameterName($field);

            $queryBuilder
                ->andWhere(sprintf($wrapCase('%s.%s') . ' IN (:%s)', $alias, $field, $valueParameter))
                ->setParameter($valueParameter, $caseSensitive ? $values : array_map('strtolower', $values));
        }

        // metadata doesn't have the field, nor an association on the field
        if (!$metadata->hasAssociation($field)) {
            return;
        }
        // If we are targeting an association, only EXACT strategy is allowed
        if ($strategy !== self::STRATEGY_EXACT) {
            $this->logger->notice('Invalid filter ignored', [
                'exception' => new InvalidArgumentException(sprintf('Association "%s" can only be matched with \'' . self::STRATEGY_EXACT . '\' strategy', $field)),
            ]);

            return;
        }

        $values                     = array_map([$this, 'getIdFromValue'], $values);
        $associationFieldIdentifier = 'id';
        $doctrineTypeField          = $this->getDoctrineFieldType($property, $resourceClass);

        if (null !== $this->identifiersExtractor) {
            $associationResourceClass   = $metadata->getAssociationTargetClass($field);
            $associationFieldIdentifier = $this->identifiersExtractor->getIdentifiersFromResourceClass($associationResourceClass)[0];
            $doctrineTypeField          = $this->getDoctrineFieldType($associationFieldIdentifier, $associationResourceClass);
        }

        if (!$this->hasValidValues($values, $doctrineTypeField)) {
            $this->logger->notice('Invalid filter ignored', [
                'exception' => new InvalidArgumentException(sprintf('Values for field "%s" are not valid according to the doctrine type.', $field)),
            ]);

            return;
        }

        $association    = $field;
        $valueParameter = $queryNameGenerator->generateParameterName($association);
        if ($metadata->isCollectionValuedAssociation($association)) {
            $associationAlias = QueryBuilderHelper::addJoinOnce($queryBuilder, $queryNameGenerator, $alias, $association);
            $associationField = $associationFieldIdentifier;
        } else {
            $associationAlias = $alias;
            $associationField = $field;
        }

        if (1 === count($values)) {
            $queryBuilder
                ->andWhere(sprintf('%s.%s = :%s', $associationAlias, $associationField, $valueParameter))
                ->setParameter($valueParameter, $values[0]);
        } else {
            $queryBuilder
                ->andWhere(sprintf('%s.%s IN (:%s)', $associationAlias, $associationField, $valueParameter))
                ->setParameter($valueParameter, $values);
        }
    }

    /**
     * Normalize the values array.
     */
    protected function normalizeValues(array $values, string $property): ?array
    {
        foreach ($values as $key => $value) {
            if (!is_int($key) || !is_string($value)) {
                unset($values[$key]);
            }
        }

        if (empty($values)) {
            $this->getLogger()->notice('Invalid filter ignored', [
                'exception' => new InvalidArgumentException(sprintf('At least one value is required, multiple values should be in "%1$s[]=firstvalue&%1$s[]=secondvalue" format', $property)),
            ]);

            return null;
        }

        return array_values($values);
    }

    /**
     * When the field should be an integer, check that the given value is a valid one.
     *
     * @param mixed|null $type
     */
    protected function hasValidValues(array $values, $type = null): bool
    {
        foreach ($values as $key => $value) {
            if (self::DOCTRINE_INTEGER_TYPE === $type && null !== $value && false === filter_var($value, FILTER_VALIDATE_INT)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Adds where clause according to the strategy.
     *
     * @throws InvalidArgumentException If strategy does not exist
     */
    protected function addWhereByStrategy(string $strategy, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $alias, string $field, $value, bool $caseSensitive)
    {
        $wrapCase       = $this->createWrapCase($caseSensitive);
        $valueParameter = $queryNameGenerator->generateParameterName($field);

        switch ($strategy) {
            case null:
            case self::STRATEGY_EXACT:
                $queryBuilder
                    ->andWhere(sprintf($wrapCase('%s.%s') . ' = ' . $wrapCase(':%s'), $alias, $field, $valueParameter))
                    ->setParameter($valueParameter, $value);
                break;
            case self::STRATEGY_PARTIAL:
                $queryBuilder
                    ->andWhere(sprintf($wrapCase('%s.%s') . ' LIKE ' . $wrapCase('CONCAT(\'%%\', :%s, \'%%\')'), $alias, $field, $valueParameter))
                    ->setParameter($valueParameter, $value);
                break;
            case self::STRATEGY_START:
                $queryBuilder
                    ->andWhere(sprintf($wrapCase('%s.%s') . ' LIKE ' . $wrapCase('CONCAT(:%s, \'%%\')'), $alias, $field, $valueParameter))
                    ->setParameter($valueParameter, $value);
                break;
            case self::STRATEGY_END:
                $queryBuilder
                    ->andWhere(sprintf($wrapCase('%s.%s') . ' LIKE ' . $wrapCase('CONCAT(\'%%\', :%s)'), $alias, $field, $valueParameter))
                    ->setParameter($valueParameter, $value);
                break;
            case self::STRATEGY_WORD_START:
                $queryBuilder
                    ->andWhere(sprintf($wrapCase('%1$s.%2$s') . ' LIKE ' . $wrapCase('CONCAT(:%3$s, \'%%\')') . ' OR ' . $wrapCase('%1$s.%2$s') . ' LIKE ' . $wrapCase('CONCAT(\'%%\', :%3$s, \'%%\')'), $alias, $field, $valueParameter))
                    ->setParameter($valueParameter, $value);
                break;
            default:
                throw new InvalidArgumentException(sprintf('strategy %s does not exist.', $strategy));
        }
    }

    /**
     * Creates a function that will wrap a Doctrine expression according to the
     * specified case sensitivity.
     *
     * For example, "o.name" will get wrapped into "LOWER(o.name)" when $caseSensitive
     * is false.
     */
    protected function createWrapCase(bool $caseSensitive): Closure
    {
        return static function (string $expr) use ($caseSensitive): string {
            if ($caseSensitive) {
                return $expr;
            }

            return sprintf('LOWER(%s)', $expr);
        };
    }

    /**
     * @return LoggerInterface|null
     */
    public function getLogger(): ?LoggerInterface
    {
        return $this->logger;
    }

    /**
     * {@inheritdoc}
     */
    protected function getType(string $doctrineType): string
    {
        switch ($doctrineType) {
            case DBALType::TARRAY:
                return 'array';
            case DBALType::BIGINT:
            case DBALType::INTEGER:
            case DBALType::SMALLINT:
                return 'int';
            case DBALType::BOOLEAN:
                return 'bool';
            case DBALType::DATE:
            case DBALType::TIME:
            case DBALType::DATETIME:
            case DBALType::DATETIMETZ:
                return DateTimeInterface::class;
            case DBALType::FLOAT:
                return 'float';
        }

        if (defined(DBALType::class . '::DATE_IMMUTABLE')) {
            switch ($doctrineType) {
                case DBALType::DATE_IMMUTABLE:
                case DBALType::TIME_IMMUTABLE:
                case DBALType::DATETIME_IMMUTABLE:
                case DBALType::DATETIMETZ_IMMUTABLE:
                    return DateTimeInterface::class;
            }
        }

        return 'string';
    }

    /**
     * Gets the ID from an IRI or a raw ID.
     */
    protected function getIdFromValue(string $value)
    {
        try {
            $item = $this->getIriConverter()->getItemFromIri($value, ['fetch_data' => false]);

            return $this->getPropertyAccessor()->getValue($item, 'id');
        } catch (InvalidArgumentException $e) {
            // Do nothing, return the raw value
        }

        return $value;
    }

    protected function getIriConverter(): IriConverterInterface
    {
        return $this->iriConverter;
    }

    protected function getPropertyAccessor(): PropertyAccessorInterface
    {
        return $this->propertyAccessor;
    }

    protected function getManagerRegistry(): ManagerRegistry
    {
        return $this->managerRegistry;
    }
}
