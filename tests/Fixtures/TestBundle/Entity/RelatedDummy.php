<?php


namespace Sungazer\Bundle\ApiPlatformFiltersBundle\Tests\Fixtures\TestBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class RelatedDummy
{
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    public $name;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    public $surname;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

}