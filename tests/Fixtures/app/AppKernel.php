<?php

declare(strict_types=1);

use ApiPlatform\Core\Bridge\Symfony\Bundle\ApiPlatformBundle;
use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use Sungazer\Bundle\ApiPlatformFiltersBundle\Tests\Fixtures\TestBundle\TestBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Bundle\TwigBundle\TwigBundle;
use Symfony\Bundle\WebProfilerBundle\WebProfilerBundle;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Routing\RouteCollectionBuilder;


/**
 * AppKernel for tests.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class AppKernel extends Kernel
{
    use MicroKernelTrait;

    public function __construct(string $environment, bool $debug)
    {
        parent::__construct($environment, $debug);

    }

    public function registerBundles(): array
    {
        $bundles = [
            new FrameworkBundle(),
            new WebProfilerBundle(),
            new TwigBundle(),
            new DoctrineBundle(),
            new ApiPlatformBundle(),
            new TestBundle(),

        ];

        return $bundles;
    }

    public function getProjectDir()
    {
        return __DIR__;
    }

    protected function configureContainer(ContainerBuilder $c, LoaderInterface $loader)
    {
        $c->setParameter('kernel.project_dir', __DIR__);

        $loader->load(__DIR__."/config/config_{$this->getEnvironment()}.yml");

        // Required to generate the doctrine service
        $c->prependExtensionConfig('doctrine', [
            'dbal' => [
            ],
        ]);
    }

    protected function configureRoutes(RouteCollectionBuilder $routes)
    {
//        $routes->import(__DIR__."/config/routing_{$this->getEnvironment()}.yml");

//        if ($_SERVER['LEGACY'] ?? false) {
//            $routes->import('@NelmioApiDocBundle/Resources/config/routing.yml', '/nelmioapidoc');
//        }
    }
}
