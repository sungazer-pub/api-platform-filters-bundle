<?php


namespace Sungazer\Bundle\ApiPlatformFiltersBundle\Tests\Doctrine\Orm\Filter;


use ApiPlatform\Core\Api\IdentifiersExtractorInterface;
use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGenerator;
use ApiPlatform\Core\Exception\InvalidArgumentException;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectRepository;
use Prophecy\Argument;
use Sungazer\Bundle\ApiPlatformFiltersBundle\Doctrine\Orm\Filter\SearchFilter;
use Sungazer\Bundle\ApiPlatformFiltersBundle\Tests\Fixtures\TestBundle\Entity\Dummy;
use Sungazer\Bundle\ApiPlatformFiltersBundle\Tests\Fixtures\TestBundle\Entity\RelatedDummy;
use Sungazer\Bundle\ApiPlatformFiltersBundle\Tests\Fixtures\TestBundle\Util\PHPUnitUtil;
use Symfony\Bridge\Doctrine\Test\DoctrineTestHelper;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

/**
 * Given the combinatorial nature of the filter, here we test the 2 functions (extractOperations, applyFilterOperation) separately, to reduce the test cases we have to test
 * @covers \Sungazer\Bundle\ApiPlatformFiltersBundle\Doctrine\Orm\Filter\SearchFilter
 */
class SearchFilterTest extends KernelTestCase
{

    protected $filterClass = SearchFilter::class;
    protected $resourceClass = Dummy::class;
    protected $alias = 'oo';
    /**
     * @var object|null
     */
    private $managerRegistry;
    /**
     * @var EntityRepository|ObjectRepository
     */
    private $repository;

    public function provideExtractOperationsData(): array
    {
        $filterFactory = [$this, 'buildSearchFilter'];

        return [
            'fullyDefined'                            => [
                [
                    'name' => ['property' => 'name'],
                ],
                ['name' => ['exact' => 'testName']],
                [
                    ['exact', 'name', 'testName']
                ],
                $filterFactory,
            ],
            'default'                                 => [
                [
                    'name' => []
                ],
                ['name' => ['exact' => 'testName']],
                [
                    ['exact', 'name', 'testName']
                ],
                $filterFactory,
            ],
            'aliased'                                 => [
                [
                    'name_aliased' => ['property' => 'name'],
                ],
                ['name_aliased' => ['exact' => 'testName']],
                [
                    ['exact', 'name', 'testName']
                ],
                $filterFactory,
            ],
            'aliased+default'                         => [
                [
                    'name_aliased' => ['property' => 'name'],
                    'name'         => []
                ],
                ['name_aliased' => ['exact' => 'testName'], 'name' => ['exact' => 'testName']],
                [
                    ['exact', 'name', 'testName'],
                    ['exact', 'name', 'testName']
                ],
                $filterFactory,
            ],
            'nested_aliased+nested_default'           => [
                [
                    'relatedName'       => ['property' => 'relatedDummy.name'],
                    'relatedDummy.name' => []
                ],
                ['relatedName' => ['exact' => 'testName'], 'relatedDummy.name' => ['exact' => 'testName']],
                [
                    ['exact', 'relatedDummy.name', 'testName'],
                    ['exact', 'relatedDummy.name', 'testName']
                ],
                $filterFactory,
            ],
            'multiple filters'                        => [
                [
                    'name_aliased'    => ['property' => 'name'],
                    'surname_aliased' => ['property' => 'surname'],
                    'name'            => [],
                    'surname'         => []
                ],
                ['name_aliased' => ['exact' => 'testName'], 'surname_aliased' => ['exact' => 'testSurname']],
                [
                    ['exact', 'name', 'testName'],
                    ['exact', 'surname', 'testSurname']
                ],
                $filterFactory,
            ],
            'multiple filters (different strategies)' => [
                [
                    'name_aliased'    => ['property' => 'name'],
                    'surname_aliased' => ['property' => 'surname'],
                    'name'            => [],
                    'surname'         => []
                ],
                ['name_aliased' => ['iexact' => 'testName'], 'surname_aliased' => ['ipartial' => 'testSurname']],
                [
                    ['iexact', 'name', 'testName'],
                    ['ipartial', 'surname', 'testSurname']
                ],
                $filterFactory,
            ],
            'single filter, multiple values'          => [
                [
                    'name_aliased'    => ['property' => 'name'],
                    'surname_aliased' => ['property' => 'surname'],
                    'name'            => [],
                    'surname'         => []
                ],
                ['name_aliased' => ['iexact' => ['testName01', 'testName02']]],
                [
                    ['iexact', 'name', ['testName01', 'testName02']],
                ],
                $filterFactory,
            ],
            'non existing property'          => [
                [
                    'name_aliased'    => ['property' => 'name'],
                    'surname_aliased' => ['property' => 'surname'],
                    'name'            => [],
                    'surname'         => []
                ],
                ['another_name' => ['iexact' => ['testName01', 'testName02']]],
                [],
                $filterFactory,
            ],
        ];
    }

    public function provideApplyFilterOperationData(): array
    {
        $filterFactory  = [$this, 'buildSearchFilter'];
        $baseFilterSpec = [
            'name_aliased'      => ['property' => 'name'],
            'surname_aliased'   => ['property' => 'surname'],
            'name'              => [],
            'surname'           => [],
            'relatedName'       => ['property' => 'relatedDummy.name'],
            'relatedDummy.name' => [],
        ];

        return [
            'exact'                  => [
                $baseFilterSpec,
                [['exact', 'name', 'testName']],
                sprintf('SELECT %s FROM %s %1$s WHERE %1$s.name = :name_p1', $this->alias, $this->resourceClass),
                ['name_p1' => 'testName'],
                $filterFactory,
            ],
            'iexact'                 => [
                $baseFilterSpec,
                [['iexact', 'name', 'testName']],
                sprintf('SELECT %s FROM %s %1$s WHERE LOWER(%1$s.name) = LOWER(:name_p1)', $this->alias, $this->resourceClass),
                ['name_p1' => 'testName'],
                $filterFactory,
            ],
            'exact (multiple)'                  => [
                $baseFilterSpec,
                [['exact', 'name', ['a','B']]],
                sprintf('SELECT %s FROM %s %1$s WHERE %1$s.name IN (:name_p1)', $this->alias, $this->resourceClass),
                ['name_p1' => ['a','B']],
                $filterFactory,
            ],
            'iexact (multiple)'                  => [
                $baseFilterSpec,
                [['iexact', 'name', ['A','B']]],
                sprintf('SELECT %s FROM %s %1$s WHERE LOWER(%1$s.name) IN (:name_p1)', $this->alias, $this->resourceClass),
                ['name_p1' => ['a','b']],
                $filterFactory,
            ],
            'partial'                => [
                $baseFilterSpec,
                [['partial', 'name', 'testName']],
                sprintf('SELECT %s FROM %s %1$s WHERE %1$s.name LIKE CONCAT(\'%%\', :name_p1, \'%%\')', $this->alias, $this->resourceClass),
                ['name_p1' => 'testName'],
                $filterFactory,
            ],
            'ipartial'               => [
                $baseFilterSpec,
                [['ipartial', 'name', 'testName']],
                sprintf('SELECT %s FROM %s %1$s WHERE LOWER(%1$s.name) LIKE LOWER(CONCAT(\'%%\', :name_p1, \'%%\'))', $this->alias, $this->resourceClass),
                ['name_p1' => 'testName'],
                $filterFactory,
            ],
            'start'                => [
                $baseFilterSpec,
                [['start', 'name', 'testName']],
                sprintf('SELECT %s FROM %s %1$s WHERE %1$s.name LIKE CONCAT(:name_p1, \'%%\')', $this->alias, $this->resourceClass),
                ['name_p1' => 'testName'],
                $filterFactory,
            ],
            'istart'               => [
                $baseFilterSpec,
                [['istart', 'name', 'testName']],
                sprintf('SELECT %s FROM %s %1$s WHERE LOWER(%1$s.name) LIKE LOWER(CONCAT(:name_p1, \'%%\'))', $this->alias, $this->resourceClass),
                ['name_p1' => 'testName'],
                $filterFactory,
            ],
            'end'                => [
                $baseFilterSpec,
                [['end', 'name', 'testName']],
                sprintf('SELECT %s FROM %s %1$s WHERE %1$s.name LIKE CONCAT(\'%%\', :name_p1)', $this->alias, $this->resourceClass),
                ['name_p1' => 'testName'],
                $filterFactory,
            ],
            'iend'               => [
                $baseFilterSpec,
                [['iend', 'name', 'testName']],
                sprintf('SELECT %s FROM %s %1$s WHERE LOWER(%1$s.name) LIKE LOWER(CONCAT(\'%%\', :name_p1))', $this->alias, $this->resourceClass),
                ['name_p1' => 'testName'],
                $filterFactory,
            ],
            'word_start'                => [
                $baseFilterSpec,
                [['word_start', 'name', 'testName']],
                sprintf('SELECT %s FROM %s %1$s WHERE %1$s.name LIKE CONCAT(:name_p1, \'%%\') OR %1$s.name LIKE CONCAT(\'%%\', :name_p1, \'%%\')', $this->alias, $this->resourceClass),
                ['name_p1' => 'testName'],
                $filterFactory,
            ],
            'iword_start'               => [
                $baseFilterSpec,
                [['iword_start', 'name', 'testName']],
                sprintf('SELECT %s FROM %s %1$s WHERE LOWER(%1$s.name) LIKE LOWER(CONCAT(:name_p1, \'%%\')) OR LOWER(%1$s.name) LIKE LOWER(CONCAT(\'%%\', :name_p1, \'%%\'))', $this->alias, $this->resourceClass),
                ['name_p1' => 'testName'],
                $filterFactory,
            ],
            // TODO Change this when we implement partial multiple filter
            'partial multiple'                => [
                $baseFilterSpec,
                [['partial', 'name', ['a','B']]],
                sprintf('SELECT %s FROM %s %1$s', $this->alias, $this->resourceClass),
                [],
                $filterFactory,
            ],
            'IRI value for relation' => [
                $baseFilterSpec,
                [['exact', 'relatedDummy', '/related_dummies/1']],
                sprintf('SELECT %s FROM %s %1$s WHERE %1$s.relatedDummy = :relatedDummy_p1', $this->alias, $this->resourceClass),
                ['relatedDummy_p1' => 1],
                $filterFactory,
            ],
            'IRI value for relation (invalid)' => [
                $baseFilterSpec,
                [['exact', 'relatedDummy', '/related_dummie/1']],
                sprintf('SELECT %s FROM %s %1$s', $this->alias, $this->resourceClass),
                [],
                $filterFactory,
            ],
            'invalid strategy for relation' => [
                $baseFilterSpec,
                [['ipartial', 'relatedDummy', '/related_dummies/1'],['iexact', 'relatedDummy', '/related_dummies/1'],['partial', 'relatedDummy', '/related_dummies/1']],
                sprintf('SELECT %s FROM %s %1$s', $this->alias, $this->resourceClass),
                [],
                $filterFactory,
            ],
            'ID value for relation'  => [
                $baseFilterSpec,
                [['exact', 'relatedDummy', '1']],
                sprintf('SELECT %s FROM %s %1$s WHERE %1$s.relatedDummy = :relatedDummy_p1', $this->alias, $this->resourceClass),
                ['relatedDummy_p1' => 1],
                $filterFactory,
            ],
            'mixed ID and IRI values for relation'  => [
                $baseFilterSpec,
                [['exact', 'relatedDummy', ['1', '/related_dummies/1']]],
                sprintf('SELECT %s FROM %s %1$s WHERE %1$s.relatedDummy IN (:relatedDummy_p1)', $this->alias, $this->resourceClass),
                ['relatedDummy_p1' => [1, 1]],
                $filterFactory,
            ],
            'mixed ID and IRI values for relation (multiple relations)'  => [
                $baseFilterSpec,
                [
                    ['exact', 'relatedDummy', ['1', '/related_dummies/1']],
                    ['exact', 'relatedDummies', ['1', '/related_dummies/1']]
                    ],
                sprintf('SELECT %s FROM %s %1$s INNER JOIN %1$s.relatedDummies relatedDummies_a1 WHERE %1$s.relatedDummy IN (:relatedDummy_p1) AND relatedDummies_a1.id IN (:relatedDummies_p2)', $this->alias, $this->resourceClass),
                [
                    'relatedDummy_p1' => [1, 1],
                    'relatedDummies_p2' => [1, 1]
                ],
                $filterFactory,
            ],
            'mixed ID and IRI values for relation (multiple relations) 2'  => [
                $baseFilterSpec,
                [
                    ['exact', 'relatedDummy', ['1', '/related_dummies/1']],
                    ['exact', 'relatedDummies', ['1']]
                ],
                sprintf('SELECT %s FROM %s %1$s INNER JOIN %1$s.relatedDummies relatedDummies_a1 WHERE %1$s.relatedDummy IN (:relatedDummy_p1) AND relatedDummies_a1.id = :relatedDummies_p2', $this->alias, $this->resourceClass),
                [
                    'relatedDummy_p1' => [1, 1],
                    'relatedDummies_p2' => 1
                ],
                $filterFactory,
            ],
            'ID value for relation (wrong type)'  => [
                $baseFilterSpec,
                [['exact', 'relatedDummy', 1]],
                sprintf('SELECT %s FROM %s %1$s', $this->alias, $this->resourceClass),
                [],
                $filterFactory,
            ],
            'nested property'        => [
                $baseFilterSpec,
                [['exact', 'relatedDummy.name', 'testName']],
                sprintf('SELECT %s FROM %s %1$s INNER JOIN %1$s.relatedDummy relatedDummy_a1 WHERE relatedDummy_a1.name = :name_p1', $this->alias, $this->resourceClass),
                ['name_p1' => 'testName'],
                $filterFactory,
            ],
            'non existing property'        => [
                $baseFilterSpec,
                [['exact', 'another_property', 'testName']],
                sprintf('SELECT %s FROM %s %1$s', $this->alias, $this->resourceClass),
                [],
                $filterFactory,
            ],
        ];
    }

    public function provideApplyTestData(): array
    {
        $filterFactory = [$this, 'buildSearchFilter'];
        $baseFilterSpec = [
            'name_aliased'      => ['property' => 'name'],
            'surname_aliased'   => ['property' => 'surname'],
            'name'              => [],
//            'surname'           => [],
            'relatedName'       => ['property' => 'relatedDummy.name'],
            'relatedDummy.name' => [],
        ];

        return [
            'exact'                    => [
                $baseFilterSpec,
                ['name' => ['exact' => 'testName'], 'another_property' => ['exact' => 'asd'], 'surname' => ['exact' => 'asd']],
                sprintf('SELECT %s FROM %s %1$s WHERE %1$s.name = :name_p1', $this->alias, $this->resourceClass),
                ['name_p1' => 'testName'],
                $filterFactory,
            ]
        ];
    }

    /**
     * @dataProvider provideExtractOperationsData
     */
    public function testExtractOperations(?array $properties, array $filterParameters, array $expectedResult, callable $factory = null)
    {
        /** @var SearchFilter $filterCallable */
        $filterCallable = $factory($this->managerRegistry, $properties);
        $result         = iterator_to_array(PHPUnitUtil::callMethod($filterCallable, 'extractOperations', [$filterParameters]));
        $this->assertEqualsCanonicalizing($expectedResult, $result);
    }

    /**
     * @dataProvider provideApplyFilterOperationData
     */
    public function testApplyFilterOperationData(?array $properties, array $operations, string $expectedDql, array $expectedParameters, callable $factory = null)
    {
        $queryBuilder = $this->repository->createQueryBuilder($this->alias);
        $qng          = new QueryNameGenerator();
        /** @var SearchFilter $filterCallable */
        $filterCallable = $factory($this->managerRegistry, $properties);
        foreach ($operations as $operation) {
            PHPUnitUtil::callMethod($filterCallable, 'applyFilterOperation', array_merge($operation, [$queryBuilder, $qng, $this->resourceClass]));
        }

        $this->assertEquals($expectedDql, $queryBuilder->getQuery()->getDQL());
        if (!$expectedParameters) {
            return;
        }

        foreach ($expectedParameters as $parameterName => $expectedParameterValue) {
            $queryParameter = $queryBuilder->getQuery()->getParameter($parameterName);

            $this->assertNotNull($queryParameter, sprintf('Expected query parameter "%s" to be set', $parameterName));
            $this->assertEqualsCanonicalizing($expectedParameterValue, $queryParameter->getValue(), sprintf('Expected query parameter "%s" to be "%s"', $parameterName, var_export($expectedParameterValue, true)));
        }
    }

    protected function setUp(): void
    {
        self::bootKernel();

        $manager               = DoctrineTestHelper::createTestEntityManager();
        $this->managerRegistry = self::$kernel->getContainer()->get('doctrine');
        $this->repository      = $manager->getRepository($this->resourceClass);
    }

    /**
     * @dataProvider provideApplyTestData
     */
    public function testApply(?array $properties, array $filterParameters, string $expectedDql, array $expectedParameters = null, callable $factory = null)
    {
        $this->doTestApply(false, $properties, $filterParameters, $expectedDql, $expectedParameters, $factory);
    }

    protected function doTestApply(bool $request, ?array $properties, array $filterParameters, string $expectedDql, array $expectedParameters, callable $filterFactory)
    {

        $queryBuilder   = $this->repository->createQueryBuilder($this->alias);
        $filterCallable = $filterFactory($this->managerRegistry, $properties);
        $filterCallable->apply($queryBuilder, new QueryNameGenerator(), $this->resourceClass, null, $request ? [] : ['filters' => $filterParameters]);

        $this->assertEquals($expectedDql, $queryBuilder->getQuery()->getDQL());

        if (!$expectedParameters) {
            return;
        }

        foreach ($expectedParameters as $parameterName => $expectedParameterValue) {
            $queryParameter = $queryBuilder->getQuery()->getParameter($parameterName);

            $this->assertNotNull($queryParameter, sprintf('Expected query parameter "%s" to be set', $parameterName));
            $this->assertEquals($expectedParameterValue, $queryParameter->getValue(), sprintf('Expected query parameter "%s" to be "%s"', $parameterName, var_export($expectedParameterValue, true)));
        }
    }

    protected function buildSearchFilter(ManagerRegistry $managerRegistry, ?array $properties = null)
    {
        $relatedDummyProphecy = $this->prophesize(RelatedDummy::class);
        $iriConverterProphecy = $this->prophesize(IriConverterInterface::class);

        $iriConverterProphecy->getItemFromIri(Argument::type('string'), ['fetch_data' => false])
            ->will(function ($args) use ($relatedDummyProphecy) {
                if (false !== strpos($args[0], '/related_dummies')) {
                    $relatedDummyProphecy->getId()->shouldBeCalled()->willReturn(1);

                    return $relatedDummyProphecy->reveal();
                }

                throw new InvalidArgumentException();
            });

        $iriConverter     = $iriConverterProphecy->reveal();
        $propertyAccessor = self::$kernel->getContainer()->get('test.property_accessor');

        $identifierExtractorProphecy = $this->prophesize(IdentifiersExtractorInterface::class);
        $identifierExtractorProphecy->getIdentifiersFromResourceClass(Argument::type('string'))->willReturn(['id']);

        return new SearchFilter($managerRegistry, $iriConverter, $propertyAccessor, static::$container->get('logger', ContainerInterface::NULL_ON_INVALID_REFERENCE), $properties, $identifierExtractorProphecy->reveal(), new CamelCaseToSnakeCaseNameConverter());
    }
}